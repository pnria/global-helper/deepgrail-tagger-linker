
# region get atoms and polarities after tagger
from Linker.utils_linker import find_pos_neg_idexes, get_atoms_batch, get_num_atoms_batch


def get_info_for_tagger(max_len_sentence, pred_categories):
    categories_batch = pred_categories
    polarities = find_pos_neg_idexes(categories_batch)
    atoms_batch = get_atoms_batch(categories_batch)
    num_atoms_batch = get_num_atoms_batch(categories_batch, max_len_sentence)

    return atoms_batch, polarities, num_atoms_batch


# endregion
