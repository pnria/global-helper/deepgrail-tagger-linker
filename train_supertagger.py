from SuperTagger.SuperTagger.SuperTagger import SuperTagger
from utils import read_supertags_csv, load_obj
from find_config import configurate_supertagger

import torch
torch.cuda.empty_cache()

dataset = 'SuperTagger/Datasets/m2_dataset_V2.csv'
index_to_super_path = 'SuperTagger/Datasets/index_to_super'
bert_model = "flaubert/flaubert_base_cased"

configurate_supertagger(dataset, index_to_super_path, bert_model, nb_sentences=1000000000)

df = read_supertags_csv(dataset)
texts = df['X'].tolist()
tags = df['Z'].tolist()

index_to_super = load_obj(index_to_super_path)

tagger = SuperTagger()
tagger.create_new_model(len(index_to_super),bert_model,index_to_super)
## If you want to upload a pretrained model
# tagger.load_weights("models/model_check.pt")
tagger.train(texts, tags, epochs=70, batch_size=16, validation_rate=0.1, 
            tensorboard=True, checkpoint=True)


