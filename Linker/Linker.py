import math
import os
import sys
import time

import torch
import torch.nn.functional as F
from torch.nn import Sequential, LayerNorm, Module, Linear, Dropout, TransformerEncoderLayer, TransformerEncoder, \
    Embedding, GELU
from torch.optim import AdamW
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import TensorDataset, random_split
from tqdm import tqdm

from Configuration import Configuration
from .AtomTokenizer import AtomTokenizer
from .PositionalEncoding import PositionalEncoding
from Linker.Sinkhorn import sinkhorn_fn_no_exp as sinkhorn
from Linker.atom_map import atom_map, atom_map_redux
from Linker.eval import measure_accuracy, SinkhornLoss
from Linker.utils_linker import FFN, get_axiom_links, get_GOAL, get_pos_idx, get_neg_idx, get_atoms_batch, \
    find_pos_neg_idexes, get_num_atoms_batch, generate_square_subsequent_mask
from SuperTagger import SuperTagger
from utils import pad_sequence, format_time, output_create_dir


class Linker(Module):
    
    # region initialization

    def __init__(self):
        super(Linker, self).__init__()

        # region parameters
        config = Configuration.read_config()
        datasetConfig = config["DATASET_PARAMS"]
        modelEncoderConfig = config["MODEL_ENCODER"]
        modelLinkerConfig = config["MODEL_LINKER"]
        dim_encoder = int(modelEncoderConfig['dim_encoder'])
        atom_vocab_size = int(datasetConfig['atom_vocab_size'])
        
        # Transformer
        self.nhead = int(modelLinkerConfig['nhead'])
        self.dim_emb_atom = int(modelLinkerConfig['dim_emb_atom'])
        self.dim_feedforward_transformer = int(modelLinkerConfig['dim_feedforward_transformer'])
        self.num_layers = int(modelLinkerConfig['num_layers'])
        # torch cat
        dropout = float(modelLinkerConfig['dropout'])
        self.dim_cat_out = int(modelLinkerConfig['dim_cat_out'])
        dim_intermediate_FFN = int(modelLinkerConfig['dim_intermediate_FFN'])
        dim_pre_sinkhorn_transfo = int(modelLinkerConfig['dim_pre_sinkhorn_transfo'])
        # sinkhorn
        self.sinkhorn_iters = int(modelLinkerConfig['sinkhorn_iters'])
        # settings
        self.max_len_sentence = int(datasetConfig['max_len_sentence'])
        self.max_atoms_in_sentence = int(datasetConfig['max_atoms_in_sentence'])
        self.max_atoms_in_one_type = int(datasetConfig['max_atoms_in_one_type'])
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        # endregion

        # Atoms embedding
        self.atoms_tokenizer = AtomTokenizer(atom_map, self.max_atoms_in_sentence)
        self.atom_map_redux = atom_map_redux
        self.padding_id = atom_map["[PAD]"]
        self.sub_atoms_type_list = list(atom_map_redux.keys())
        self.atom_encoder = Embedding(atom_vocab_size, self.dim_emb_atom, padding_idx=self.padding_id)
        self.atom_encoder.weight.data.uniform_(-0.1, 0.1)
        self.position_encoder = PositionalEncoding(self.dim_emb_atom, dropout, max_len=self.max_atoms_in_sentence)
        encoder_layer = TransformerEncoderLayer(d_model=self.dim_emb_atom, nhead=self.nhead,
                                                dim_feedforward=self.dim_feedforward_transformer, dropout=dropout)
        self.transformer = TransformerEncoder(encoder_layer, num_layers=self.num_layers)

        # Concatenation with word embedding
        dim_cat = dim_encoder + self.dim_emb_atom
        self.linker_encoder = Sequential(
            Linear(dim_cat, self.dim_cat_out),
            GELU(),
            Dropout(dropout),
            LayerNorm(self.dim_cat_out, eps=1e-8))

        # Division into positive and negative
        self.pos_transformation = Sequential(
            FFN(self.dim_cat_out, dim_intermediate_FFN, dropout, d_out=dim_pre_sinkhorn_transfo),
            LayerNorm(dim_pre_sinkhorn_transfo, eps=1e-8))
        self.neg_transformation = Sequential(
            FFN(self.dim_cat_out, dim_intermediate_FFN, dropout, d_out=dim_pre_sinkhorn_transfo),
            LayerNorm(dim_pre_sinkhorn_transfo, eps=1e-8))

        # Learning
        self.cross_entropy_loss = SinkhornLoss()
        self.optimizer = AdamW(self.parameters(), lr=0.0001)
        self.scheduler = StepLR(self.optimizer, step_size=5, gamma=0.5)
        self.to(self.device)

    def load_weights(self, model_file):
        print("#" * 15)
        try:
            params = torch.load(model_file, map_location=self.device)
            self.atom_encoder.load_state_dict(params['atom_encoder'])
            self.position_encoder.load_state_dict(params['position_encoder'])
            self.transformer.load_state_dict(params['transformer'])
            self.linker_encoder.load_state_dict(params['linker_encoder'])
            self.pos_transformation.load_state_dict(params['pos_transformation'])
            self.neg_transformation.load_state_dict(params['neg_transformation'])
            self.cross_entropy_loss.load_state_dict(params['cross_entropy_loss'])
            self.optimizer = params['optimizer']
            print("\n The loading checkpoint was successful ! \n")
        except Exception as e:
            print("\n/!\ Can't load checkpoint model /!\ because :\n\n " + str(e), file=sys.stderr)
            raise e
        print("#" * 15)

    #endregion

    # region training

    def make_sinkhorn_inputs(self, bsd_tensor, positional_ids, atom_type):
        """
        :param bsd_tensor:
            Tensor of shape batch size \times sequence length \times feature dimensionality.
        :param positional_ids:
            A List of batch_size elements, each being a List of num_atoms LongTensors.
            Each LongTensor in positional_ids[b][a] indexes the location of atoms of type a in sentence b.
        :param atom_type:
        :return:
        """

        return torch.stack([torch.stack([bsd_tensor.select(0, index=i).select(0, index=int(atom)).to(self.device)
                                         if atom != -1 else torch.zeros(self.dim_cat_out, device=self.device)
                                         for atom in sentence])
                            for i, sentence in enumerate(positional_ids[:, self.atom_map_redux[atom_type], :])])

    def forward(self, batch_num_atoms_per_word, batch_atoms, batch_pos_idx, batch_neg_idx, sents_embedding):
        r"""
        Args:
            batch_num_atoms_per_word : (batch_size, len_sentence) flattened categories
            batch_atoms : atoms tok
            batch_pos_idx : (batch_size, atom_vocab_size, max atom in one cat) flattened categories polarities
            batch_neg_idx : (batch_size, atom_vocab_size, max atom in one cat) flattened categories polarities
            sents_embedding : (batch_size, len_sentence, dim_encoder) output of BERT for context
        Returns:
            link_weights : atom_vocab_size, batch-size, max_atoms_in_one_cat, max_atoms_in_one_cat) log probabilities
        """
        # repeat embedding word for each atom in word with a +1 for sep
        sents_embedding_repeat = pad_sequence(
            [torch.repeat_interleave(input=sents_embedding[i], repeats=batch_num_atoms_per_word[i], dim=0)
             for i in range(len(sents_embedding))], max_len=self.max_atoms_in_sentence, padding_value=0)

        # atoms emebedding
        src_key_padding_mask = torch.eq(batch_atoms, self.padding_id)
        src_mask = generate_square_subsequent_mask(self.max_atoms_in_sentence).to(self.device)
        atoms_embedding = self.atom_encoder(batch_atoms) * math.sqrt(self.dim_emb_atom)
        atoms_embedding = self.position_encoder(atoms_embedding)
        atoms_embedding = atoms_embedding.permute(1, 0, 2)
        atoms_embedding = self.transformer(atoms_embedding, src_mask,
                                           src_key_padding_mask=src_key_padding_mask)
        atoms_embedding = atoms_embedding.permute(1, 0, 2)

        # cat
        atoms_sentences_encoding = torch.cat([sents_embedding_repeat, atoms_embedding], dim=2)
        atoms_encoding = self.linker_encoder(atoms_sentences_encoding)

        # linking per atom type
        batch_size, atom_vocab_size, _ = batch_pos_idx.shape
        link_weights = torch.zeros(atom_vocab_size, batch_size, self.max_atoms_in_one_type // 2,
                                   self.max_atoms_in_one_type // 2, device=self.device)
        for atom_type in list(atom_map_redux.keys()):
            pos_encoding = self.make_sinkhorn_inputs(atoms_encoding, batch_pos_idx, atom_type)
            neg_encoding = self.make_sinkhorn_inputs(atoms_encoding, batch_neg_idx, atom_type)

            pos_encoding = self.pos_transformation(pos_encoding)
            neg_encoding = self.neg_transformation(neg_encoding)

            weights = torch.bmm(pos_encoding, neg_encoding.transpose(2, 1))
            link_weights[self.atom_map_redux[atom_type]] = sinkhorn(weights, iters=self.sinkhorn_iters)

        return F.log_softmax(link_weights, dim=3)

    def train_epoch(self, training_dataloader, Supertagger):
        r""" Train epoch

        Args:
            training_dataloader : DataLoader from torch , contains atoms, polarities, axiom_links, sents_tokenized, sents_masks
        Returns:
             accuracy on validation set
             loss on train set
        """
        self.train()

        # Reset the total loss for this epoch.
        epoch_loss = 0
        accuracy_train = 0
        t0 = time.time()

        # For each batch of training data...
        with tqdm(training_dataloader, unit="batch") as tepoch:
            for batch in tepoch:
                # Unpack this training batch from our dataloader
                batch_num_atoms = batch[0].to(self.device)
                batch_atoms_tok = batch[1].to(self.device)
                batch_pos_idx = batch[2].to(self.device)
                batch_neg_idx = batch[3].to(self.device)
                batch_true_links = batch[4].to(self.device)
                batch_sentences_tokens = batch[5].to(self.device)
                batch_sentences_mask = batch[6].to(self.device)

                self.optimizer.zero_grad()

                # get sentence embedding from BERT which is already trained
                output = Supertagger.forward(batch_sentences_tokens, batch_sentences_mask)

                # Run the Linker on the atoms
                logits_predictions = self(batch_num_atoms, batch_atoms_tok, batch_pos_idx, batch_neg_idx,
                                          output['word_embedding'])
                linker_loss = self.cross_entropy_loss(logits_predictions, batch_true_links)
                # Perform a backward pass to calculate the gradients.
                epoch_loss += float(linker_loss)
                linker_loss.backward()

                # This is to help prevent the "exploding gradients" problem.
                # torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=5.0, norm_type=2)

                # Update parameters and take a step using the computed gradient.
                self.optimizer.step()

                pred_axiom_links = torch.argmax(logits_predictions, dim=3)
                accuracy_train += measure_accuracy(batch_true_links, pred_axiom_links)

        self.scheduler.step()

        # Measure how long this epoch took.
        training_time = format_time(time.time() - t0)
        avg_train_loss = epoch_loss / len(training_dataloader)
        avg_accuracy_train = accuracy_train / len(training_dataloader)

        return avg_train_loss, avg_accuracy_train, training_time

    #endregion

    # region evaluation

    def eval_epoch(self, dataloader, Supertagger):
        r"""Average the evaluation of all the batch.

        Args:
            dataloader: contains all the batch which contain the tokenized sentences, their masks and the true symbols
        """
        self.eval()
        accuracy_average = 0
        loss_average = 0
        with torch.no_grad():
            for step, batch in enumerate(dataloader):
                batch_num_atoms = batch[0].to(self.device)
                batch_atoms_tok = batch[1].to(self.device)
                batch_pos_idx = batch[2].to(self.device)
                batch_neg_idx = batch[3].to(self.device)
                batch_true_links = batch[4].to(self.device)
                batch_sentences_tokens = batch[5].to(self.device)
                batch_sentences_mask = batch[6].to(self.device)

                output = Supertagger.forward(batch_sentences_tokens, batch_sentences_mask)

                logits_predictions = self(batch_num_atoms, batch_atoms_tok, batch_pos_idx, batch_neg_idx, output['word_embedding'])  # atom_vocab, batch_size, max atoms in one type, max atoms in one type
                axiom_links_pred = torch.argmax(logits_predictions, dim=3)  # atom_vocab, batch_size, max atoms in one type

                accuracy = measure_accuracy(batch_true_links, axiom_links_pred)
                loss = self.cross_entropy_loss(logits_predictions, batch_true_links)

                accuracy_average += accuracy
                loss_average += float(loss)

        return loss_average / len(dataloader), accuracy_average / len(dataloader)

    #endregion
