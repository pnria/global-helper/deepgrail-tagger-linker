atom_map = \
    {'cl_r': 0,
     "pp": 1,
     'n': 2,
     's_ppres': 3,
     's_whq': 4,
     's_q': 5,
     'np': 6,
     's_inf': 7,
     's_pass': 8,
     'pp_a': 9,
     'pp_par': 10,
     'pp_de': 11,
     'cl_y': 12,
     'txt': 13,
     's': 14,
     's_ppart': 15,
     "[SEP]":16,
     '[PAD]': 17
     }

atom_map_redux = {
    'cl_r': 0,
    'pp': 1,
    'n': 2,
    'np': 3,
    'cl_y': 4,
    'txt': 5,
    's': 6
}
