import torch
from utils import pad_sequence


class AtomTokenizer(object):
    r"""
    Tokenizer for the atoms with padding
    """
    def __init__(self, atom_map, max_atoms_in_sentence):
        self.atom_map = atom_map
        self.max_atoms_in_sentence = max_atoms_in_sentence
        self.inverse_atom_map = {v: k for k, v in self.atom_map.items()}
        self.pad_token = '[PAD]'
        self.pad_token_id = self.atom_map[self.pad_token]

    def __len__(self):
        return len(self.atom_map)

    def convert_atoms_to_ids(self, atom):
        r"""
        Convert a atom to its id
        :param atom: atom string
        :return: atom id
        """
        return self.atom_map[str(atom)]

    def convert_sents_to_ids(self, sentences):
        r"""
        Convert sentences to ids
        :param sentences: List of atoms in a sentence
        :return: List of atoms'ids
        """
        return torch.as_tensor([self.convert_atoms_to_ids(atom) for atom in sentences])

    def convert_batchs_to_ids(self, batchs_sentences):
        r"""
        Convert a batch of sentences of atoms to the ids
        :param batchs_sentences: batch of sentences atoms
        :return: list of list of atoms'ids
        """
        return torch.as_tensor(pad_sequence([self.convert_sents_to_ids(sents) for sents in batchs_sentences],
                                            max_len=self.max_atoms_in_sentence, padding_value=self.pad_token_id))

    def convert_ids_to_atoms(self, ids):
        r"""
        Translate id to atom
        :param ids: atom id
        :return: atom string
        """
        return [self.inverse_atom_map[int(i)] for i in ids]
