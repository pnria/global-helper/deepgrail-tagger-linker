# DeepGrail

This repository contains a Python implementation of BertForTokenClassification using TLGbank data to develop
part-of-speech taggers and supertaggers.

This code was designed to work with the [DeepGrail Linker](https://gitlab.irit.fr/pnria/global-helper/deepgrail-linker)
to provide a wide coverage syntactic and semantic parser for French. But the Tagger is independent, you can use it for your own tags.


## Structure

```
.
├── Datasets                      # TLGbank data
└──  SuperTagger                  # BertForTokenClassification
        ├── SuperTagger.py        # Main class
        ├── Tagging_bert_model.py # Bert model
        ├── SymbolTokenizer       # Tags tokenizer
        ├── SentencesTokenizer    # Words tokenizer
        └── helpers               # utils
```




