def categorical_accuracy(preds, truth):
    """
    Calculates how often predictions match argmax labels.
    preds: batch of prediction. (argmax)
    truth: batch of truth label.
    @return: scoring of batch prediction. (Categorical accuracy values)
    """
    good_label = 0
    nb_label = 0
    for i in range(len(truth)):
        sublist_truth = truth[i]
        sublist_preds = preds[i]
        for j in range(len(sublist_truth)):
            if sublist_truth[j] != 0:
                if sublist_truth[j] == sublist_preds[j]:
                    good_label += 1
                nb_label += 1
    return good_label / nb_label
