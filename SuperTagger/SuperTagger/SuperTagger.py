import os
import sys
import time

import torch
import transformers
from torch.optim import Adam
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import TensorDataset, random_split
from tqdm import tqdm
from transformers import AutoTokenizer
from transformers import logging

from Configuration import Configuration
from .SentencesTokenizer import SentencesTokenizer
from .SymbolTokenizer import SymbolTokenizer
from .Tagging_bert_model import Tagging_bert_model
from .eval import categorical_accuracy
from utils import format_time, output_create_dir


logging.set_verbosity(logging.ERROR)


# region Class

class SuperTagger:
    """
    Implements the SuperTagger to assign each word a supertag (also named symbol). A supertag is a tree of tags such as np, s, ...

    Attributes:
    -----------
    max_len_sentence : int
        Maximum length of sentence, equals to the maximum number of supertags
    index_to_tags : dic
    num_label : int
        number of possible supertags 
    bert_name : 
        name of BERT model
    sent_tokenizer ! Tokenizer
        Tokenize words to word_token
    tags_tokenizer : Tokenizer
        Tokenize supertag to supertag_token
    model : TokenClassifier
        Model for classification of tokens. Classify word_token to supertag_token.
    optimizer : Optimizer
        Optimizer to repropagate gradients
    epoch_i : int
        Current number of epoch
    device : Device
        CPU or cuda
    trainable : bool
    model_load : bool
    """

    # region Instanciation

    def __init__(self):
        """
        Python implementation of BertForTokenClassification using TLGbank data to develop supertaggers.
        """
        config = Configuration.read_config()
        datasetConfig = config["DATASET_PARAMS"]
        self.max_len_sentence = int(datasetConfig['max_len_sentence'])

        self.index_to_tags = None
        self.num_label = None
        self.bert_name = None
        self.sent_tokenizer = None
        self.tags_tokenizer = None
        self.model = None

        self.optimizer = None
        self.scheduler = None

        self.epoch_i = 0
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.trainable = False
        self.model_load = False

    def load_weights(self, model_file):
        """
        Loads an SupperTagger saved with SupperTagger.__checkpoint_save() (during a train) from a file.

        Parameters:
        -----------
            model_file: 
                path of .pt save of model
        """
        self.trainable = False

        print("#" * 20)
        print("\n Loading model for supertagger ...")
        try:
            params = torch.load(model_file, map_location=self.device)
            args = params['args']
            self.bert_name = args['bert_name']
            self.index_to_tags = args['index_to_tags']
            self.num_label = len(self.index_to_tags)
            self.model = Tagging_bert_model(self.bert_name, self.num_label)
            self.tags_tokenizer = SymbolTokenizer(self.index_to_tags)
            self.sent_tokenizer = SentencesTokenizer(transformers.AutoTokenizer.from_pretrained(
                                                    self.bert_name,
                                                    do_lower_case=True), 
                                                    self.max_len_sentence)
            self.model.load_state_dict(params['state_dict'])
            self.optimizer = params['optimizer']
            self.scheduler = StepLR(self.optimizer, step_size=10, gamma=0.5)
            # self.epoch_i = args['epoch']
            print("\n The loading checkpoint was successful ! \n")
            print("\tBert model : ", self.bert_name)
            print("\tLast epoch : ", self.epoch_i)
            print()
        except Exception as e:
            print("\n/!\ Can't load checkpoint model /!\ because :\n\n " + str(e), file=sys.stderr)
            raise e
        print("#" * 20)

        self.model_load = True
        self.trainable = True

    def create_new_model(self, num_label, bert_name, index_to_tags):
        """
        Instantiation and parameterization of a new bert model.

        Parameters:
        -----------
            num_label: 
                number of diferent labels (tags)
            bert_name: 
                name of model available on Hugging Face `<https://huggingface.co/models>`
            index_to_tags: 
                Dict for convert ID to tags
        """
        assert len(
            index_to_tags) == num_label, f" len(index_to_tags) : {len(index_to_tags)} must be equels with num_label: {num_label}"

        self.model = Tagging_bert_model(bert_name, num_label + 1)
        index_to_tags = {k + 1: v for k, v in index_to_tags.items()}
        # <unk> is used for the pad AND unknown tags
        index_to_tags[0] = '<unk>'

        self.index_to_tags = index_to_tags
        self.bert_name = bert_name
        self.sent_tokenizer = SentencesTokenizer(AutoTokenizer.from_pretrained(
                                                bert_name,
                                                do_lower_case=True),
                                                self.max_len_sentence)
        
        self.optimizer = Adam(params=self.model.parameters(), lr=1e-5, eps=1e-8)
        self.scheduler = StepLR(self.optimizer, step_size=10, gamma=0.9)
        self.tags_tokenizer = SymbolTokenizer(index_to_tags)
        self.trainable = True
        self.model_load = True

    # endregion Instanciation

    # region Usage

    def predict(self, sentences):
        """
        Predict and convert sentences in tags (depends on the dictation given when the model was created)

        Parameters:
        -----------
            sentences: list of sentences : list[str] OR one sentences : str
        
        Returns:
        --------
            tags prediction for all sentences (no argmax tags, convert tags, embedding layer of bert )
        """
        assert self.trainable or self.model is None, "Please use the create_new_model(...) or load_weights(...) " \
                                                        "function before the predict, the model is not integrated "
        assert type(sentences) == str or type(sentences) == list, "param sentences: list of sentences : list[" \
                                                                       "str] OR one sentences : str "
        sentences = [sentences] if type(sentences) == str else sentences

        self.model.eval()
        with torch.no_grad():
            sents_tokenized_t, sents_mask_t = self.sent_tokenizer.fit_transform_tensors(sentences)

            self.model = self.model.cpu()

            output = self.model.predict((sents_tokenized_t, sents_mask_t))

            return output['logit'], self.tags_tokenizer.convert_ids_to_tags(torch.argmax(output['logit'], dim=2).detach())

    def forward(self, b_sents_tokenized, b_sents_mask):
        """
        Forward to the model
        """
        with torch.no_grad():
            output = self.model.predict((b_sents_tokenized, b_sents_mask))
            return output

    def train(self, sentences, tags, validation_rate=0.1, epochs=20, batch_size=16,
              tensorboard=False,
              checkpoint=False):
        """
        Starts the training of the model, either new or previously loaded

        Parameters:
        -----------
            sentences: list of sentences for train (X)
            tags: list of tags for train (Y)
            validation_rate: percentage of validation data [0-1]
            epochs: number of epoch (50 recommended)
            batch_size:  number of sample in batch (32 recommended, attention to memory)
            tensorboard: use tensorboard for see loss and accuracy
            checkpoint: save the model after each epoch
        """
        assert self.trainable or self.model is None, "Please use the create_new_model(...) or load_weights(...) function before the train, the model is not integrated"

        assert len(sentences) == len(
            tags), f" num of sentences (X): {len(sentences)} must be equals with num of labels " \
                   f"(Y): {len(tags)} "

        if checkpoint or tensorboard:
            checkpoint_dir, writer = output_create_dir()

        training_dataloader, validation_dataloader = self.__preprocess_data(batch_size, sentences, tags,
                                                                            1 - validation_rate)
        epochs = epochs - self.epoch_i
        self.model = self.model.to(self.device)
        self.model.train()

        for epoch_i in range(0, epochs):
            print("")
            print('======== Epoch {:} / {:} ========'.format(epoch_i+1, epochs))
            print('Training...')

            # Train
            epoch_acc, epoch_loss, training_time = self.__train_epoch(training_dataloader)

            # Validation
            if validation_rate > 0.0:
                eval_accuracy, eval_loss, nb_eval_steps = self.__eval_epoch(validation_dataloader)

            print("")
            print(f'Epoch: {epoch_i+1:02} | Epoch Time: {training_time}')
            print(f'\tTrain Loss: {epoch_loss:.3f} | Train Acc: {epoch_acc * 100:.2f}%')
            if validation_rate > 0.0:
                print(f'\tVal Loss: {eval_loss:.3f} | Val Acc: {eval_accuracy * 100:.2f}%')

            if tensorboard:
                writer.add_scalars(f'Accuracy', {
                    'Train': epoch_acc}, epoch_i+1)
                writer.add_scalars(f'Loss', {
                    'Train': epoch_loss}, epoch_i+1)
                if validation_rate > 0.0:
                    writer.add_scalars(f'Accuracy', {
                        'Validation': eval_accuracy}, epoch_i+1)
                    writer.add_scalars(f'Loss', {
                        'Validation': eval_loss}, epoch_i+1)

            self.epoch_i += 1

            if checkpoint:
                self.__checkpoint_save(path=os.path.join(checkpoint_dir, 'model_check.pt'))

    # endregion Usage

    # region Private

    def __preprocess_data(self, batch_size, sentences, tags,
                          validation_rate):
        """
        Create torch dataloader for training
        
        Parameters:
        -----------
            batch_size: number of sample in batch
            sentences: list of sentences for train (X)
            tags: list of tags for train (Y)
            validation_rate: percentage of validation data [0-1]

        Returns:
        --------
            training dataloader, validation dataloader
        """
        validation_dataloader = None

        sents_tokenized_t, sents_mask_t = self.sent_tokenizer.fit_transform_tensors(sentences)
        tags_t = self.tags_tokenizer.convert_batchs_to_ids(tags, sents_tokenized_t)
        dataset = TensorDataset(sents_tokenized_t, sents_mask_t, tags_t)

        train_size = int(validation_rate * len(dataset))
        print('{:>5,} training samples'.format(train_size))

        if validation_rate < 1:
            val_size = len(dataset) - train_size
            train_dataset, val_dataset = random_split(dataset, [train_size, val_size])
            print('{:>5,} validation samples'.format(val_size))
            validation_dataloader = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=True)
        else:
            train_dataset = dataset
        training_dataloader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        return training_dataloader, validation_dataloader

    def __train_epoch(self, training_dataloader):
        """
        Train on epoch
        
        Parameters:
        -----------
            training_dataloader: dataloader of training data

        Returns:
        --------
            epoch accuracy, epoch loss, training time
        """
        self.model.train()
        epoch_loss = 0
        epoch_acc = 0
        t0 = time.time()
        i = 0
        with tqdm(training_dataloader, unit="batch") as tepoch:
            for batch in tepoch:
                # Convert to device
                b_sents_tokenized = batch[0].to(self.device)
                b_sents_mask = batch[1].to(self.device)
                targets = batch[2].to(self.device)
                self.optimizer.zero_grad()

                output = self.model((b_sents_tokenized, b_sents_mask, targets))
                loss = output['loss']

                predictions = torch.argmax(output['logit'], dim=2).detach().cpu().numpy()
                label_ids = targets.cpu().numpy()

                acc = categorical_accuracy(predictions, label_ids)

                loss.backward()

                epoch_acc += acc
                epoch_loss += loss.item()

                self.optimizer.step()
                i += 1
                
        self.scheduler.step()

        # Measure how long this epoch took.
        training_time = format_time(time.time() - t0)

        epoch_acc = epoch_acc / i
        epoch_loss = epoch_loss / i

        return epoch_acc, epoch_loss, training_time

    def __eval_epoch(self, validation_dataloader):
        """
        Validation on epoch

        Parameters:
        -----------
            validation_dataloader:  dataloader of validation data

        Returns: 
        --------
            epoch accuracy, epoch loss, num step
        """
        self.model.eval()
        eval_loss = 0
        eval_accuracy = 0
        nb_eval_steps, nb_eval_examples = 0, 0
        with torch.no_grad():
            print("Start eval")
            for step, batch in enumerate(validation_dataloader):
                # Convert to device
                b_sents_tokenized = batch[0].to(self.device)
                b_sents_mask = batch[1].to(self.device)
                b_symbols_tokenized = batch[2].to(self.device)

                output = self.model((b_sents_tokenized, b_sents_mask, b_symbols_tokenized))
                loss = output['loss']

                predictions = torch.argmax(output['logit'], dim=2).detach().cpu().numpy()
                label_ids = b_symbols_tokenized.cpu().numpy()

                accuracy = categorical_accuracy(predictions, label_ids)
                eval_loss += loss.item()
                eval_accuracy += accuracy
                nb_eval_examples += b_sents_tokenized.size(0)
                nb_eval_steps += 1

            eval_loss = eval_loss / nb_eval_steps
            eval_accuracy = eval_accuracy / nb_eval_steps
        return eval_accuracy, eval_loss, nb_eval_steps

    def __checkpoint_save(self, path='/model_check.pt'):
        """
        Save the model with good parameters

        Parameters:
        -----------
            path: poth and name for save
        """
        self.model.cpu()
        # print('save model parameters to [%s]' % path, file=sys.stderr)

        torch.save({
            'args': dict(bert_name=self.bert_name, index_to_tags=self.index_to_tags, epoch=self.epoch_i),
            'state_dict': self.model.state_dict(),
            'optimizer': self.optimizer,
        }, path)
        self.model.to(self.device)

    # endregion Private

# endregion Class