
class SentencesTokenizer():
    """
    Tokenizer for sentences : Based on a pretrained tokenzer

    Atributes:
    ----------
        tokenizer : Tokenizer
            Pretrained Tokenizer
        max_length : 
            Maximal length of a sentence (i.e maximum number of words)
    """

    def __init__(self, tokenizer, max_length):
        """
        Parameters :
        ------------
            tokenizer (PretrainedTokenizer): Tokenizer that tokenizes text 
            max_length : Maximal length of a sentence
        """
        self.tokenizer = tokenizer
        self.max_length = max_length

    def fit_transform(self, sents):
        """
        Tokenizes the given sentences
        """
        temp = self.tokenizer(sents, padding=True)
        
        return temp["input_ids"], temp["attention_mask"]

    def fit_transform_tensors(self, sents):
        """
        Tokenizes the sentences and returns tensor
        """
        temp = self.tokenizer(sents, padding='max_length', truncation=True, return_tensors = 'pt', max_length=self.max_length)

        return temp["input_ids"], temp["attention_mask"]

    def convert_ids_to_tokens(self, inputs_ids, skip_special_tokens=False):
        """
        Decodes a sentence.
        """
        return self.tokenizer.batch_decode(inputs_ids, skip_special_tokens=skip_special_tokens)
