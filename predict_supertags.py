from SuperTagger.SuperTagger.SuperTagger import SuperTagger
from SuperTagger.SuperTagger.eval import categorical_accuracy

# region data
a_s = "85,6 % de membres du PCF ."
tags_s = [['dr(0,np,n)', 'n', 'dr(0,dl(0,np,np),n)', 'n', 'dr(0,dl(0,n,n),n)', 'n', 'dl(0,np,txt)']]
# endregion


# region model
tagger = SuperTagger()
model = "models/flaubert_super_98_V2_50e.pt"
tagger.load_weights(model)
# endregion


# region prediction
_, pred_convert = tagger.predict(a_s)

print("Model : ", model)

print("\tLen Text           : ", len(a_s.split()))
print("\tLen tags           : ", len(tags_s[0]))
print("\tLen pred_convert   : ", len(pred_convert[0]))
print()
print("\tText               : ", a_s)
print()
print("\tTags               : ", tags_s[0])
print()
print("\tPred_convert       : ", pred_convert[0])
print()
print("\tScore              :", f"{categorical_accuracy(pred_convert, tags_s)*100}%" )
# endregion