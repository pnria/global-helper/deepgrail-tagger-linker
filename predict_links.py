from NeuralProofNet.NeuralProofNet import NeuralProofNet
from postprocessing import draw_sentence_output

if __name__== '__main__':
      # region data
      a_s = ["( 1 ) parmi les huit \" partants \" acquis ou potentiels , MM. Lacombe , Koehler et Laroze ne sont pas membres du PCF ."]
      tags_s = [['let', 'dr(0,s,s)', 'let', 'dr(0,dr(0,s,s),np)', 'dr(0,np,n)', 'dr(0,n,n)', 'let', 'n', 'let', 'dl(0,n,n)',
            'dr(0,dl(0,dl(0,n,n),dl(0,n,n)),dl(0,n,n))', 'dl(0,n,n)', 'let', 'dr(0,np,np)', 'np', 'dr(0,dl(0,np,np),np)',
            'np', 'dr(0,dl(0,np,np),np)', 'np', 'dr(0,dl(0,np,s),dl(0,np,s))', 'dr(0,dl(0,np,s),np)', 'dl(1,s,s)', 'np',
            'dr(0,dl(0,np,np),n)', 'n', 'dl(0,s,txt)']]
      # endregion

      # region model
      model_tagger = "models/flaubert_super_98_V2_50e.pt"
      neuralproofnet = NeuralProofNet(model_tagger)
      model = "models/saved_linker.pt"
      neuralproofnet.linker.load_weights(model)
      # endregion

      #categories, links = neuralproofnet.predict_without_categories(a_s)
      links = neuralproofnet.predict_with_categories(a_s, tags_s)
      
      idx=0
      draw_sentence_output(a_s[idx].split(" "), tags_s[idx], links[:,idx,:].numpy())
