from Linker import *
from NeuralProofNet.NeuralProofNet import NeuralProofNet
from find_config import configurate_linker
from utils import read_links_csv

import torch
torch.cuda.empty_cache()

dataset = 'Datasets/gold_dataset_links.csv'
model_tagger = "models/flaubert_super_98_V2_50e.pt"

configurate_linker(dataset, model_tagger, nb_sentences=1000000000)

df_axiom_links = read_links_csv(dataset)

neural_proof_net = NeuralProofNet(model_tagger)
neural_proof_net.train_neuralproofnet(df_axiom_links, validation_rate=0.1, epochs=25, pretrain_linker_epochs=25, batch_size=16,
                                      checkpoint=True, tensorboard=True)